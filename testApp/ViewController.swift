//
//  ViewController.swift
//  testApp
//
//  Created by Newarpunk on 1/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, StudentStateDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var present = 0
    var absent = 0
    var late = 0
    var leave = 0
    
    var allData = DataArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func totalCountClicked(_ sender: UIButton) {
//        DispatchQueue.main.async {
//            self.allData = DataArray()
//            self.tableView.reloadData()
//            self.present = 0
//            self.absent = 0
//            self.late = 0
//            self.leave = 0
//        }
        let alert = UIAlertController(title: "Count", message: "Total Present: \(present) \n Total Absent: \(absent) \n Total late: \(late) \n On Leave: \(leave)", preferredStyle: .alert)
        let addAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(addAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let totalCount = allData.dataSource.count
        return totalCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        let model = allData.dataSource[indexPath.item]
        
        cell.delegateValue = self
        cell.indexPath = indexPath
        cell.cellConfigure(dataModel: model)
        
        return cell
    }
    
    
    func buttonClicked(lastChosen: DataModel.StudentState?, state: DataModel.StudentState, indexPath: IndexPath?) {
        
        if let indexPath = indexPath {
            let model = allData.dataSource[indexPath.item]
            model.state = state
            
            if let l = lastChosen {
                switch l {
                case .present:
                    present -= 1
                    
                case .absent:
                    absent -= 1
                    
                case .leave:
                    leave -= 1
                    
                case .late:
                    late -= 1
                    
                case .none:
                    break
                }
            }
            
            if state == .present {
                present += 1
                
            } else if state == .absent {
                absent += 1
                
            } else if state == .late {
                late += 1
                
            } else if state == .leave {
                leave += 1
                
            }
            
        }
    }
    
    
}

//for i in 0..<totalCount {
//                if i == indexPath.item{
//                    if state == .present {
////                        valueHere(countValue: indexPath.item)
//                        selectedData.append("present")
////                        if selectedData.count > 1 {
////                            if selectedData.last == "present" {
////                                selectedData.removeLast()
////                            }
////                        }
//
//
////                        present += 1
////                        print(present)
//                        return
//                    } else if state == .absent {
////                        valueHere(countValue: indexPath.item)
//                        selectedData.append("absent")
////                        absent += 1
////                        print(absent)
//                        return
//                    } else if state == .late {
////                        valueHere(countValue: indexPath.item)
//                        selectedData.append("late")
////                        late += 1
////                        print(late)
//                        return
//                    } else if state == .leave {
////                        valueHere(countValue: indexPath.item)
//                        selectedData.append("leave")
////                        leave += 1
////                        print(leave)
//                        return
//                    }
//                    return
//                }
//            }
