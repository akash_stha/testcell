//
//  info.swift
//  testApp
//
//  Created by Newarpunk on 1/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import Foundation

class DataModel {
    
    enum StudentState {
        case present
        case absent
        case leave
        case late
        case none
    }
    
    var name: String
    var state: StudentState
    
    init(name: String, state: StudentState) {
        self.name = name
        self.state = state
    }
    
}
