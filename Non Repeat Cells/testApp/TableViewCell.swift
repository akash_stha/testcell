//
//  TableViewCell.swift
//  testApp
//
//  Created by Newarpunk on 1/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

protocol StudentStateDelegate {
    func buttonClicked(state: DataModel.StudentState, lastChosen: DataModel.StudentState?, indexPath: IndexPath?)
}

class TableViewCell : UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var onPresent: UIButton!
    @IBOutlet weak var onAbsent: UIButton!
    @IBOutlet weak var onLeave: UIButton!
    @IBOutlet weak var onLate: UIButton!
    
    var delegateValue : StudentStateDelegate?
    var indexPath : IndexPath?
    
    var state: DataModel.StudentState? = nil
    var lastChosen: DataModel.StudentState? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        state = nil
        lastChosen = nil
    }
    
    @IBAction func onClicked(_ sender: UIButton) {
        
        if let last = state {
            switch last {
            case .present:
                lastChosen = .present
            case .absent:
                lastChosen = .absent
            case .leave:
                lastChosen = .leave
            case .late:
                lastChosen = .late
            case .none:
                break
            }
        }
        
        if sender.tag == 0 {
            state = .present
            setStudentAttendanceType(state: .present)
            delegateValue?.buttonClicked(state: .present, lastChosen: lastChosen, indexPath: indexPath)
            return
        } else if sender.tag == 1 {
            state = .absent
            setStudentAttendanceType(state: .absent)
            delegateValue?.buttonClicked(state: .absent, lastChosen: lastChosen, indexPath: indexPath)
            return
        } else if sender.tag == 2 {
            state = .leave
            setStudentAttendanceType(state: .leave)
            delegateValue?.buttonClicked(state: .leave, lastChosen: lastChosen, indexPath: indexPath)
            return
        } else if sender.tag == 3 {
            state = .late
            setStudentAttendanceType(state: .late)
            delegateValue?.buttonClicked(state: .late, lastChosen: lastChosen, indexPath: indexPath)
            return
        }
//        switch sender.tag {
//        case 0:
//            state = .present
//            setStudentAttendanceType(state: .present)
//            delegateValue?.buttonClicked(state: .present, lastChosen: lastChosen, indexPath: indexPath)
//
//        case 1:
//            state = .absent
//            setStudentAttendanceType(state: .absent)
//            delegateValue?.buttonClicked(state: .absent, lastChosen: lastChosen, indexPath: indexPath)
//
//        case 2:
//            state = .leave
//            setStudentAttendanceType(state: .leave)
//            delegateValue?.buttonClicked(state: .leave, lastChosen: lastChosen, indexPath: indexPath)
//
//        case 3:
//            state = .late
//            setStudentAttendanceType(state: .late)
//            delegateValue?.buttonClicked(state: .late, lastChosen: lastChosen, indexPath: indexPath)
//
//        default:
//            print("do nothing")
//        }
    }
    
    func cellConfigure(dataModel: DataModel){
        nameLabel.text = dataModel.name
        setStudentAttendanceType(state: dataModel.state)
    }
    
    func setStudentAttendanceType(state: DataModel.StudentState) {
        switch state {
        case .none:
            onPresent.backgroundColor = .groupTableViewBackground
            onPresent.setTitleColor(UIColor.black, for: .normal)
            onAbsent.backgroundColor = .groupTableViewBackground
            onAbsent.setTitleColor(UIColor.black, for: .normal)
            onLeave.backgroundColor = .groupTableViewBackground
            onLeave.setTitleColor(UIColor.black, for: .normal)
            onLate.backgroundColor = .groupTableViewBackground
            onLate.setTitleColor(UIColor.black, for: .normal)
        case.present:
            onPresent.backgroundColor = .darkGray
            onPresent.setTitleColor(UIColor.white, for: .normal)
            onAbsent.backgroundColor = .groupTableViewBackground
            onAbsent.setTitleColor(UIColor.black, for: .normal)
            onLeave.backgroundColor = .groupTableViewBackground
            onLeave.setTitleColor(UIColor.black, for: .normal)
            onLate.backgroundColor = .groupTableViewBackground
            onLate.setTitleColor(UIColor.black, for: .normal)
        case .absent:
            onPresent.backgroundColor = .groupTableViewBackground
            onPresent.setTitleColor(UIColor.black, for: .normal)
            onAbsent.backgroundColor = .darkGray
            onAbsent.setTitleColor(UIColor.white, for: .normal)
            onLeave.backgroundColor = .groupTableViewBackground
            onLeave.setTitleColor(UIColor.black, for: .normal)
            onLate.backgroundColor = .groupTableViewBackground
            onLate.setTitleColor(UIColor.black, for: .normal)
        case .late:
            onPresent.backgroundColor = .groupTableViewBackground
            onPresent.setTitleColor(UIColor.black, for: .normal)
            onAbsent.backgroundColor = .groupTableViewBackground
            onAbsent.setTitleColor(UIColor.black, for: .normal)
            onLeave.backgroundColor = .groupTableViewBackground
            onLeave.setTitleColor(UIColor.black, for: .normal)
            onLate.backgroundColor = .darkGray
            onLate.setTitleColor(UIColor.white, for: .normal)
        case .leave:
            onPresent.backgroundColor = .groupTableViewBackground
            onPresent.setTitleColor(UIColor.black, for: .normal)
            onAbsent.backgroundColor = .groupTableViewBackground
            onAbsent.setTitleColor(UIColor.black, for: .normal)
            onLeave.backgroundColor = .darkGray
            onLeave.setTitleColor(UIColor.white, for: .normal)
            onLate.backgroundColor = .groupTableViewBackground
            onLate.setTitleColor(UIColor.black, for: .normal)
        }
    }
}


