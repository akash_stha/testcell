//
//  ViewController.swift
//  testApp
//
//  Created by Newarpunk on 1/28/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class ViewController: UIViewController, StudentStateDelegate {
 
    @IBOutlet weak var tableView: UITableView!
    
    var allDataArray = DataArray()
    
    var present: Int = 0
    var absent: Int = 0
    var late: Int = 0
    var leave: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func totalCount(_ sender: UIButton) {
        let alert = UIAlertController.init(title: "Count", message: "Present: \(present) \n Absent: \(absent) \n Leave: \(leave) \n Late: \(late)", preferredStyle: .alert)
        let cancelAction = UIAlertAction.init(title: "Ok", style: .default, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allDataArray.dataSouce.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        let model = allDataArray.dataSouce[indexPath.item]
        cell.cellConfigure(dataModel: model)
        cell.delegateValue = self
        cell.indexPath = indexPath
        return cell
    }
    
    func buttonClicked(state: DataModel.StudentState, lastChosen: DataModel.StudentState?, indexPath: IndexPath?) {
        if let indexPath = indexPath {
            let model = allDataArray.dataSouce[indexPath.item]
            model.state = state
            
            if let last = lastChosen {
                switch last {
                case .present:
                    present -= 1
                case .absent:
                    absent -= 1
                case .leave:
                    leave -= 1
                case .late:
                    late -= 1
                case .none:
                    break
                }
            }
            
            if state == .present {
                present += 1
            } else if state == .absent {
                absent += 1
            } else if state == .leave {
                leave += 1
            } else if state == .late {
                late += 1
            }
        }
    }

}
